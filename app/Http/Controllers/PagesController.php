<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use App\Models\Moto;

class PagesController extends Controller
{
        
    public function index() {
        //$data = Moto::all();

        //$motos = DB::select('select matricula, marca, modelo, ano, km, valor, distrito, concelho, tel from motos, proprietarios where motos.proprietario = proprietarios.id and motos.venda = true order by marca, modelo, distrito, concelho;');
        $motos = DB::table('motos')
                     ->join('proprietarios', 'motos.proprietario', '=', 'proprietarios.id')
                     ->select('motos.matricula', 'motos.marca', 'motos.modelo', 'motos.ano', 'motos.km', 'motos.valor', 'proprietarios.distrito', 'proprietarios.concelho', 'proprietarios.tel')
                     ->where('motos.venda', true)
                     ->orderBy('motos.marca', 'asc')
                     ->orderBy('motos.modelo', 'asc')
                     ->orderBy('proprietarios.distrito', 'asc')
                     ->orderBy('proprietarios.concelho', 'asc')
                     ->get();

        //$motos = array(1,2,3);
        //$motos = json_decode($motos);
        //foreach ($data as $moto) {
        //  echo json_encode($moto) . "<br>";
        //}
        //echo $motos;
        //echo '<br><br><br><br>';
        //echo $data;
        //return view('welcome')->with('motos', $motos);
        return view('welcome', compact('motos'));
   	}

   	public function contactos() {
        return view('contactos');
   	}

   	public function sobre() {
        return view('sobre');
   	}

    public static function fcp() {
   		return "FCP";
   	}
}
