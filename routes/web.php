<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 		 'App\Http\Controllers\PagesController@index');
Route::get('/contactos', 'App\Http\Controllers\PagesController@contactos');
Route::get('/sobre', 	 'App\Http\Controllers\PagesController@sobre');

Route::post('/enviar',   'App\Http\Controllers\MessageController@enviar');
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});
/*
Route::get('/', function () {
    return view('welcome');
});
*/
