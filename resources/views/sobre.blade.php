@php
use App\Http\Controllers\PagesController;
@endphp

@extends('page')

<!--
@section('body')
<body>
@stop
-->

@section('link_inicio')
<a class="nav-link" href="/">INÍCIO</a>
@stop

@section('link_contactos')
<a class="nav-link" href="/contactos">CONTACTOS</a>
@stop

@section('link_sobre')
<a class="nav-link disabled" href="/sobre">SOBRE</a>
@stop

@section('content')
	<div class="row">
    	<div class="col-sm-12" style="padding:0px; border-top-style:solid; border-width:5px; border-color:#343a40;">
    		{{ PagesController::fcp() }}
    	</div>
    </div>
@stop
