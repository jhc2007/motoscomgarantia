@extends('page')

<!-- 
@section('script')
<script>
  function setMsgSucesso() {
    //$('#div_msg_sucesso').text("Mensagem enviada com sucesso!");
    setTimeout(function() { 
      $('#div_msg_sucesso').text("");
    }, 2000);
  }     
</script>
@stop
-->

<!--
@section('body')
  @if(session()->has('message'))
  <body onload="setMsgSucesso()">
  @else
  <body>
  @endif
@stop
-->

@section('link_inicio')
<a class="nav-link" href="/">INÍCIO</a>
@stop

@section('link_contactos')
<a class="nav-link disabled" href="/contactos">CONTACTOS</a>
@stop

@section('link_sobre')
<a class="nav-link" href="/sobre">SOBRE</a>
@stop

@section('content')
<div class="row">
<div class="col-sm-12" style="padding:0px; border-top-style:solid; border-width:5px; border-color:#343a40;">
<br>
<div class="row">
  <div class="col-md-6">
      <form action="/enviar" method="post">
        @csrf
        <input class="form-control" name="name" placeholder="Nome" required><br>
        <input class="form-control" type="email" name="email" placeholder="Email" required><br>
        <textarea class="form-control" name="message" placeholder="Mensagem" style="height:150px;" required></textarea>
        <br>
        <div class="row">
          @if(session()->has('message'))
          <div class="col-8 text-success" id="div_msg_sucesso">{{ session()->get('message') }}</div>
          <script type="text/javascript">
            function setMsgSucesso() {
              //$('#div_msg_sucesso').text("Mensagem enviada com sucesso!");
              setTimeout(function() { 
                $('#div_msg_sucesso').text("");
              }, 2000);
            }     
            setMsgSucesso();
          </script> 
          <div class="col-4" align="right"><input class="btn btn-dark" type="submit" value="Enviar" id="btn_enviar"></div>
          @else
          <div class="col-12" align="right"><input class="btn btn-dark" type="submit" value="Enviar" id="btn_enviar"></div>
          @endif
        </div>
        <br><br>
      </form>
  </div>
  <div class="col-md-3"></div>
  <div class="col-md-3" align="center">
    <b>MOTOS com GARANTIA</b><br>
    938 471 113<br>
    <!-- <a href="mailto:info@motoscomgarantia.com">info@motoscomgarantia.com</a><br> -->
    info@motoscomgarantia.com<br>
  </div>
</div>
</div>
</div>
@stop

