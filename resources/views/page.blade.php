<!DOCTYPE html>
<html lang="en">
<head>
  <title>MOTOS com GARANTIA</title>
  <meta charset="utf-8">
  <!-- <link rel="shortcut icon" href="ico/logo.ico"/> -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <!-- @yield('script') -->
</head>

<!-- @yield('body') -->
<boby>

<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
<!--<nav class="navbar fixed-top navbar-expand-md bg-dark navbar-dark">-->
  <!-- Brand -->
  <a class="navbar-brand" href=".">MOTOS com GARANTIA</a>

  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Navbar links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        @yield('link_inicio')
      </li>
      <li class="nav-item">
        @yield('link_contactos')
      </li>
      <li class="nav-item">
        @yield('link_sobre')
      </li>
    </ul>
  </div>
</nav>

<br>
<br>

<div class="container" style="padding:20px;">
  <!-- <div class="row"> -->

      @yield('content')

  <!-- </div> -->
</div>
<!--
<?php
//echo phpinfo();
?>
-->
</body>
</html>