@extends('page')

<!--
@section('body')
<body>
@stop
-->

@section('link_inicio')
<a class="nav-link disabled" href="/">INÍCIO</a>
@stop

@section('link_contactos')
<a class="nav-link" href="/contactos">CONTACTOS</a>
@stop

@section('link_sobre')
<a class="nav-link" href="/sobre">SOBRE</a>
@stop

@section('content')

	@php

		$carousel_num = 0;

	@endphp


	@foreach ($motos as $moto)

		@php

			$path = public_path($moto->matricula);
			$files = File::allFiles($path);
			$carousel_num++;
			$foto_num = 0;
		
		@endphp

		<div class="row" style="padding-bottom:15px;">
			<div class="col-sm-8" style="background-color:#bfbfbf; padding:0px; border-top-style:solid; border-width:5px; border-color:#343a40;">

    			<div id="carousel{{ $carousel_num }}" class="carousel slide" data-ride="carousel{{ $carousel_num }}">
    			  	<ol class="carousel-indicators">
						@foreach($files as $file)

							@if(pathinfo($file, PATHINFO_EXTENSION) == "jpg")

								@if($foto_num == 0)
    			    				<li data-target="#carousel{{ $carousel_num }}" data-slide-to="0" class="active"></li>
    			    			@else
    			    				<li data-target="#carousel{{ $carousel_num }}" data-slide-to="{{ $foto_num }}"></li>
    			    			@endif

    			    			@php
    			    				$foto_num++;
    			    			@endphp

    			    		@endif

						@endforeach
    			  	</ol>
    			  	<div class="carousel-inner">
						
                     	@php
	    			  		$foto_num = 0;
						@endphp

                     	@foreach($files as $file)

							@if(pathinfo($file, PATHINFO_EXTENSION) == "jpg")

								@if($foto_num == 0)
					    			<div class="carousel-item active">
					    		@else
					    			<div class="carousel-item">
					    		@endif
    			      			
	    			      			<img class="d-block w-100" src="{{ $moto->matricula }}/{{ pathinfo($file, PATHINFO_BASENAME) }}">
    			    			</div>

    			    			@php
    			    				$foto_num++;
    			    			@endphp

    			    		@endif

    			    	@endforeach


    			  	</div>
    			  	<a class="carousel-control-prev" href="#carousel{{ $carousel_num }}" role="button" data-slide="prev">
    			    	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
    			    	<span class="sr-only">Previous</span>
    			  	</a>
    			  	<a class="carousel-control-next" href="#carousel{{ $carousel_num }}" role="button" data-slide="next">
    			    	<span class="carousel-control-next-icon" aria-hidden="true"></span>
    			    	<span class="sr-only">Next</span>
	    		 	</a>
	    		</div>
		
			</div>
	
			<div class="col-sm-4" style="background-color:#bfbfbf; border-top-style:solid; border-width:5px; border-color:#343a40; padding:15px;">
	
	  			<b>{{ $moto->marca }} {{ $moto->modelo }}</b><br>
	  			{{ $moto->ano }}<br>
	  			{{ $moto->km }} km<br>
	  			{{ $moto->valor }} €<br>
	  			{{ $moto->distrito }}<br>
	  			{{ $moto->concelho }}<br>
	  			{{ $moto->tel }}
	  	
	  		</div>
	  	</div>

	@endforeach

@stop